import UIKit

protocol ShowUpdateView {
    func showUpdateView(elemntIndex:Int)
}

protocol ShowDeleteDialog {
    func showDeleteDialog(alert:UIViewController)
}

class ColorTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    var colorArray = [Color]()
    var colorDataManager = ColorDataManager.colorDataManager
    var colorDescTV:ColorDescTextView!

    var showUpdateViewDelegate:ShowUpdateView?
    var showDeleteDialogDelegate:ShowDeleteDialog?

    func initalize(colorArray:[Color], textView:ColorDescTextView) {
        self.colorArray = colorArray
        self.colorDescTV = textView

        self.delegate = self
        self.dataSource = self
    }

    // MARK: - Color table delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if colorArray.count < 1 {
            tableView.backgroundView?.accessibilityLabel = "No colors"
            
            var emptyMessage = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
            emptyMessage.text = "There is no color"
            emptyMessage.textColor = UIColor.red
            emptyMessage.textAlignment = NSTextAlignment.center
            tableView.backgroundView = emptyMessage
            tableView.separatorStyle = .none

        } else {
            tableView.backgroundView = nil
            tableView.separatorStyle = .singleLine

        }
        return colorArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColorCell", for: indexPath) as? ColorTableCell

        if indexPath.row == 0 {
            colorDescTV.text = colorArray[indexPath.row].description
            colorDescTV.backgroundColor = colorArray[indexPath.row].color as! UIColor
        }
        
        cell?.setCircleLabelAndTitleLabel(color: colorArray[indexPath.row].color as! UIColor, title: colorArray[indexPath.row].title!)

        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let color = colorArray[indexPath.row]
        
        colorDescTV.setText(txt: color.description!)
        colorDescTV.setBackground(color: color.color! as! UIColor)
    }
    // MARK: - Reset text field
    func clearColorDescTV() {
        self.colorDescTV.text = ""
        self.colorDescTV.backgroundColor = UIColor.white
    }
    // MARK: - Add,Delete and Update table data
    func updateColorArray(colorArray:[Color]) {
        self.colorArray = colorArray
        clearColorDescTV()
        self.reloadData()
    }

    func deleteColorAt(index:Int) {
        
        let alert = UIAlertController(title: "Delete", message: "Delete the color ?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction!) in
            self.colorDataManager.deleteColor(index: index)
            self.colorArray.remove(at: index)
            self.clearColorDescTV()
            self.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "Cancle", style: UIAlertAction.Style.default, handler: nil))

        self.showDeleteDialogDelegate?.showDeleteDialog(alert: alert)
    }

}

extension ColorTableView: FirebaseColorDelegate {
    func dataReaded(colors: [Color]) {
        self.colorArray = colors
        self.reloadData()
    }
}

