import UIKit

class ColorTableCell: UITableViewCell {
    
    @IBOutlet weak var colorCircleView: UIView!
    
    @IBOutlet weak var colorTitleLabel: UILabel!
    
    var colorDataManager = ColorDataManager.colorDataManager
    var tableView:ColorTableView? {
        return self.next(of: ColorTableView.self)
    }
    
    var indexPath: IndexPath? {
        return self.tableView?.indexPath(for: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteColor(_ sender: Any) {
        tableView?.deleteColorAt(index: indexPath!.row)
    }
    
    @IBAction func updateColor(_ sender: Any) {
        tableView!.showUpdateViewDelegate?.showUpdateView(elemntIndex: indexPath!.row)
    }
    
    func setCircleLabelAndTitleLabel(color:UIColor, title:String) {
        colorCircleView.layer.cornerRadius = 10
        colorCircleView.clipsToBounds = true
        colorCircleView.backgroundColor = color
        
        colorTitleLabel.text = title
    }

}

extension UIResponder {
    /**
     * Returns the next responder in the responder chain cast to the given type, or
     * if nil, recurses the chain until the next responder is nil or castable.
     */
    func next<U: UIResponder>(of type: U.Type = U.self) -> U? {
        return self.next.flatMap({ $0 as? U ?? $0.next() })
    }
}
