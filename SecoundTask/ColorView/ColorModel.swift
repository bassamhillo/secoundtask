import Foundation
import UIKit
import CoreData
import FirebaseDatabase

protocol FirebaseColorDelegate {
    func dataReaded(colors:[Color])
}

class ColorDataManager {

    let persistentContainer:NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MyData")
        container.loadPersistentStores {(storeDescription, error) in
            if let error = error {
                fatalError("Loading of store faild \(error)")
            }
        }
        return container
    }()
    
    var colors = [Color]()
    var databaseRef = Database.database().reference()
    var delegate:FirebaseColorDelegate?
    
    static var colorDataManager = ColorDataManager()

    func getColorsArray() -> [Color] {
        if !colors.isEmpty {
            return colors
        }
        
        databaseRef.child("Colors").observeSingleEvent(of: .value, with: { (snapshot) in
          // Get user value
            let databaseColor = snapshot.value as? [String: Any]
            for color in databaseColor! {
                let colorValue = color.value as? [String: Any]
                
                let newColor = Color()
                let background = colorValue!["background"] as! String
                newColor.color = UIColor(hexString: background)

                newColor.title = colorValue!["title"] as? String
                newColor.description = colorValue!["description"] as? String
                            
                self.colors.append(newColor)
            }
            self.delegate?.dataReaded(colors: self.colors)
    })
        return colors
    }

    func addNewColor(title:String, desc:String, background:UIColor) -> Color {
      /*
        let contex = persistentContainer.viewContext

        let colorDetails = NSEntityDescription.insertNewObject(forEntityName: "ColorDetails", into: contex) as! ColorDetails
        
        colorDetails.title = title
        colorDetails.colorDescription = desc
        colorDetails.color = background
        
        do {
            try contex.save()
        } catch let createError {
            fatalError("\(createError)")
        }
*/
        let color = Color()
        color.title = title
        color.description = desc
        color.color = background
        
        colors.append(color)
        
        let colorBackground = color.color?.toHexString()
        
        let colorRef = databaseRef.child("Colors/\(color.title ?? "")")
        colorRef.child("title").setValue(color.title)
        colorRef.child("description").setValue(color.description)
        colorRef.child("background").setValue(colorBackground)

        return color
    }

    func deleteColor(index:Int) {
        /*
        let contex = persistentContainer.viewContext
        contex.delete(colors[index])
        colors.remove(at: index)
        
        do {
            try contex.save()
        } catch let deleteError {
            fatalError("\(deleteError)")
        }
       */
        let colorTitle = colors[index].title

        colors.remove(at: index)
        
        databaseRef.child("Colors").child(colorTitle!).removeValue()

    }
    
    func updateColor(at:Int, title:String, desc:String, background:UIColor) {
        /*
        let contex = persistentContainer.viewContext

        let color = colors[at]
        color.title = title
        color.colorDescription = desc
        color.color = background
        
        do {
            try contex.save()
        } catch let updateError {
            fatalError("\(updateError)")
        }
 */
        let color = colors[at]
        
        databaseRef.child("Colors").child(color.title!).removeValue()
        
        color.title = title
        color.description = desc
        color.color = background
        
        colors[at] = color
        
        let colorBackground = background.toHexString()
        
        let newColorRef = databaseRef.child("Colors/\(color.title ?? "")")
        newColorRef.child("title").setValue(title)
        newColorRef.child("description").setValue(desc)
        newColorRef.child("background").setValue(colorBackground)
    }
    
    func getColorAt(at: Int) -> Color {
        return colors[at]
    }
}

extension UIColor {
    convenience init(hexString:String) {
        let hexString:NSString = hexString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}
