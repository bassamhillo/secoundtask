//
//  ColorDescTextView.swift
//  SecoundTask
//
//  Created by Julia on 7/7/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class ColorDescTextView: UITextView {
    
    func setText(txt:String) {
        self.text = txt
    }
    
    func setBackground(color:UIColor) {
        self.backgroundColor = color
    }
}
