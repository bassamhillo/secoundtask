//
//  ViewController.swift
//  SecoundTask
//
//  Created by Julia on 7/6/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController {

    @IBOutlet weak var colorTable: ColorTableView!
    @IBOutlet weak var colorDesc: ColorDescTextView!
    @IBOutlet weak var seperator: UIView!
    
    
    var colorDataManager = ColorDataManager.colorDataManager
    var colorArray = [Color]()
    var orientation = UIDevice.current.orientation


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        colorTable.showUpdateViewDelegate = self
        colorTable.showDeleteDialogDelegate = self
        
        colorDataManager.delegate = colorTable
        colorArray = colorDataManager.getColorsArray()
        
        colorTable.initalize(colorArray: colorArray, textView: colorDesc)
        
        // Select the first row of table
        let indexPath = IndexPath(row: 0, section: 0)
        colorTable.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        
        //
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(openColorDetailsView))

        let screenSize: CGFloat = UIScreen.main.scale
        if orientation.isLandscape {
            seperator.frame.size.width = 2 * screenSize
        } else {
            seperator.frame.size.height = 2 * screenSize
        }

    }
    
    // MARK: - Open add new color view controller
    @IBAction func openColorDetailsView(_ sender: Any) {
        let addColorView = storyboard?.instantiateViewController(identifier: "ColorDetailsView") as! AddNewColorViewViewController
        let navCtrl = UINavigationController(rootViewController: addColorView)
        navCtrl.modalPresentationStyle = .fullScreen
        addColorView.delegate = self
        navigationController?.pushViewController(addColorView, animated: true)
    }

}

// MARK: - Add new color delegate
extension ColorViewController: RefreshColorTable{
    func RefreshTable() {
        colorArray = colorDataManager.getColorsArray()
        colorTable.updateColorArray(colorArray: colorArray)
    }
}
// MARK: - Show update view and delete dialog delegate
extension ColorViewController: ShowUpdateView, ShowDeleteDialog {
    func showDeleteDialog(alert:UIViewController) {
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUpdateView(elemntIndex: Int) {
        let addColorView = storyboard?.instantiateViewController(identifier: "ColorDetailsView") as! AddNewColorViewViewController
        let navCtrl = UINavigationController(rootViewController: addColorView)

        addColorView.delegate = self
        addColorView.isForUpdate = true
        addColorView.updateColorIndex = elemntIndex
        
        navCtrl.modalPresentationStyle = .fullScreen
        
        navigationController?.pushViewController(addColorView, animated: true)
    }

}
