//
//  AddTableCell.swift
//  SecoundTask
//
//  Created by Julia on 7/12/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation
import UIKit

enum CellType:Int, CaseIterable {
    case title = 0
    case description = 1
    case background = 2
}

class UIObject {
    var type:CellType?
    var textField:UITextField?
    var validationLabel:UILabel?

    func setType(type:CellType?) {
        self.type = type
    }

    func getType() -> CellType? {
        return self.type
    }
    
    func setTextField(textField:UITextField?) {
        self.textField = textField
    }

    func getTextField() -> UITextField? {
        return self.textField
    }
    
    func setValidationLabel(validationLabel:UILabel?) {
        self.validationLabel = validationLabel
    }

    func getValidationLabel() -> UILabel? {
        return self.validationLabel
    }
    
    func setValidationLabelMessage(message:String) {
        self.validationLabel!.text = message
    }
    
    func setTextFieldValue(value:String) {
        self.textField?.text = value
    }
    
}
