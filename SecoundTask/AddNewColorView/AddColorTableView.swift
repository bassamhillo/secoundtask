import UIKit
import EFColorPicker

enum TestTable:CaseIterable {
    case test
}

class AddColorTableView: UITableView, UITableViewDataSource, UITableViewDelegate {

    var sections = [Section]()
    var items = [UIObject]()
    let sectionOne = Section()
    let sectionTwo = Section()

    var viewController:AddNewColorViewViewController?
    var isForUpdate:Bool?
    var updateIndex:Int?
    
    var backgroundIndexPath:IndexPath?
    
    let colorDataManager = ColorDataManager.colorDataManager
    var color: UIColor? {
        didSet{
            if items.count >= CellType.background.rawValue {
                items[CellType.background.rawValue].getTextField()!.backgroundColor = color
            }
            }
    }
    
    var colorToUpdate:Color?
    
    let doneBtn: UIBarButtonItem = UIBarButtonItem(
        title: NSLocalizedString("Add", comment: ""),
        style: UIBarButtonItem.Style.done,
        target: self,
        action: #selector(addOrUpdate)
    )
    
    let backBtn: UIBarButtonItem = UIBarButtonItem(
        title: NSLocalizedString("Back", comment: ""),
        style: UIBarButtonItem.Style.done,
        target: self,
        action: #selector(doneDismiss)
    )

    func initialize(viewController:AddNewColorViewViewController, isForUpdate:Bool, updateIndex: Int!) {
        self.viewController = viewController
        self.isForUpdate = isForUpdate
        self.updateIndex = updateIndex
        
        var rightBtnTitle = "Add"

        if isForUpdate {
            let currentColor = colorDataManager.getColorAt(at: updateIndex)
            colorToUpdate = currentColor

            color = currentColor.color as! UIColor
            
            rightBtnTitle = "Update"
        }
        
        // Set sections
        
        sectionOne.title = "Info"
        
        for type in CellType.allCases {
            let uiObject = UIObject()
            uiObject.setType(type: type)
            sectionOne.items.append(uiObject)
        }
        
        
        sectionTwo.title = "test"
        
        let uiObject = UIObject()
        uiObject.setType(type: .title)
        
        sectionTwo.items.append(uiObject)

        sections.append(sectionOne)
        sections.append(sectionTwo)


        self.delegate = self
        self.dataSource = self
        
        viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(title: rightBtnTitle, style: .plain, target: self, action: #selector(addOrUpdate))
        
    }
    
    // MARK: - Add and update button action
    @objc func addOrUpdate() {
        guard CellType.title.rawValue < items.count && CellType.description.rawValue < items.count && CellType.background.rawValue < items.count else {
            return
        }
        let colorTitleTF = items[CellType.title.rawValue].getTextField()!
        let colorDescTF = items[CellType.description.rawValue].getTextField()!
        
        let colorTitleValidation = items[CellType.title.rawValue].getValidationLabel()!
        let colorDescValidation = items[CellType.description.rawValue].getValidationLabel()!
        let colorBackgroundValidation = items[CellType.background.rawValue].getValidationLabel()!

        if colorTitleTF.text == "" {
            colorTitleValidation.text = "Please set the color title."
        } else {
            colorTitleValidation.text = ""

        }
        if colorDescTF.text == "" {
            colorDescValidation.text = "Please set the color description."
        } else {
            colorDescValidation.text = ""
        }
        if color == nil {
            colorBackgroundValidation.text = "Choose a color please."

        } else {
            colorBackgroundValidation.text = ""
        }
        if isForUpdate != nil && isForUpdate! == true {
            if(colorTitleTF.text != "" && colorDescTF.text != "" && color != nil){
                colorDataManager.updateColor(at: updateIndex!, title: colorTitleTF.text!, desc: colorDescTF.text!, background: color!)
                    viewController!.delegate?.RefreshTable()
                    viewController!.navigationController?.popViewController(animated: true)
            }
        } else {
            if(colorTitleTF.text != "" && colorDescTF.text != "" && color != nil){
                _ = colorDataManager.addNewColor(title: colorTitleTF.text!, desc: colorDescTF.text!, background: self.color!)
                viewController!.delegate?.RefreshTable()
                viewController!.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func doneDismiss() {
            viewController!.navigationController?.popViewController(animated: true)
    }
    
    @objc func doneColorPicker() {
        viewController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table delegates
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "NewColorCell", for: indexPath) as? NewColorTableCell
        let item = sections[indexPath.section].items[indexPath.row] as! UIObject
        item.setTextField(textField: cell?.getTextField())
        item.setValidationLabel(validationLabel: cell?.validationLabel)
        switch item.getType() {
        case .title:
            item.textField?.placeholder = "title"
            if isForUpdate != nil && isForUpdate! && colorToUpdate != nil {
                item.textField?.text = colorToUpdate!.title
            }
        case .description:
            item.textField?.placeholder = "description"
            if isForUpdate != nil && isForUpdate! && colorToUpdate != nil {
                item.textField?.text = colorToUpdate!.description
            }
        case .background:
            item.textField?.placeholder = "background"
            item.textField?.isEnabled = false
            if isForUpdate != nil && isForUpdate! && colorToUpdate != nil {
                item.textField?.backgroundColor = colorToUpdate!.color as! UIColor
            }
            backgroundIndexPath = indexPath

        default:
            break
        }
        items.append(item)
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == backgroundIndexPath {
            OpenColorPicker()
        }
    }
}



// MARK: - Color picker delegate
extension AddColorTableView: EFColorSelectionViewControllerDelegate {
    func colorViewController(_ colorViewCntroller: EFColorSelectionViewController, didChangeColor color: UIColor) {
        self.color = color
    }
}

// MARK: - Open color picker delegate
extension AddColorTableView {
    func OpenColorPicker() {
             let colorSelectionController = EFColorSelectionViewController()
             let navCtrl = UINavigationController(rootViewController: colorSelectionController)

             let doneBtn: UIBarButtonItem = UIBarButtonItem(
                 title: NSLocalizedString("Done", comment: ""),
                 style: UIBarButtonItem.Style.done,
                 target: self,
                 action: #selector(doneColorPicker)
             )

             colorSelectionController.delegate = self
             colorSelectionController.navigationItem.rightBarButtonItem = doneBtn
             navCtrl.modalPresentationStyle = .fullScreen

        viewController!.present(navCtrl, animated: true, completion: nil)

    }
    
    
}
