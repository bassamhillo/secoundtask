import UIKit

protocol RefreshColorTable {
    func RefreshTable()
}

class AddNewColorViewViewController: UIViewController {
    
    @IBOutlet weak var newColorFieldTableView: AddColorTableView!

    var isForUpdate:Bool?
    var updateColorIndex:Int?
    var delegate:RefreshColorTable?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isForUpdate != nil && isForUpdate! == true {
            newColorFieldTableView.initialize(viewController: self, isForUpdate: true, updateIndex: updateColorIndex!)
        } else {
            newColorFieldTableView.initialize(viewController: self, isForUpdate: false, updateIndex: nil)
        }
    }
    

}




