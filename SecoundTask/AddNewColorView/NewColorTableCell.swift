//
//  NewColorTableCell.swift
//  SecoundTask
//
//  Created by Julia on 7/9/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class NewColorTableCell: UITableViewCell {
    

    @IBOutlet weak var newColorTableTF: UITextField!
    @IBOutlet weak var validationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValidationLabel(message:String) {
        validationLabel.text = message
    }
    
    func getTextField() -> UITextField {
        return newColorTableTF
    }

}
