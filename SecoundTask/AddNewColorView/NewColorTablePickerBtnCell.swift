import UIKit

protocol OpenColorPicker {
    func openColorPicker()
}

class NewColorTablePickerBtnCell: UITableViewCell {

    var delegate:OpenColorPicker?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func openColorPicker(_ sender: Any) {
        delegate?.openColorPicker()
    }
    

}
