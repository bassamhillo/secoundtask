//
//  Section.swift
//  SecoundTask
//
//  Created by Julia on 7/13/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation

class Section {
    var title:String?
    var items = [Any]()
}
